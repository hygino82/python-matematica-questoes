from math import log10

pv = 20000.0
fv = 21000.0
i = 2 / 100.0
primeiroLog = log10(fv/pv)
segundoLog = log10(1+i)
n = primeiroLog/segundoLog

print(f'pv = {pv:.2f}')
print(f'fv = {fv:.2f}')
print(f'i = {i:.4f}')
print(f'primeiroLog = {primeiroLog}')
print(f'segundoLog = {segundoLog}')
print(f'n = {n}')
