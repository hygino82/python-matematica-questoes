from math import pow

i = 6 / 100.0
n = 6

diferenca = 2633.36
taxa1 = 1 + i * n
taxa2 = pow(1 + i, n)
taxa_dif = taxa2-taxa1
valor = diferenca / (taxa2-taxa1)
juros = (taxa2-1) * valor

print(f'i = {i}')
print(f'n = {n}')
print(f'diferença = {diferenca}')
print(f'taxa1 = {taxa1}')
print(f'taxa2 = {taxa2}')
print(f'taxa2 - taxa 1 = {taxa_dif}')
print(f'valor = {valor}')
print(f'juros = {juros}')
