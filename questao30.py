from math import pow

pv = 200000.0
i = 0.7 / 100.0
n = 6
potencia = pow(1 + i, n)
fv = pv * pow(1 + i, n)

print(f'pv = {pv:.2f}')
print(f'i = {i:.4f}')
print(f'n = {n}')
print(f'potencia = {potencia}')
print(f'fv = {fv:.2f}')
