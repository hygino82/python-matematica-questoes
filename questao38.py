from math import pow

i = 6.0 / 100.0
n = 6
diferenca = 2633.36
taxaSimples = 1 + i * n
taxaComposto = pow(1 + i, n)
taxaDiferenca = taxaComposto - taxaSimples
capital = diferenca / taxaDiferenca
montante = capital * taxaComposto
juros = montante - capital

print(f'i = {i}')
print(f'n = {n}')
print(f'Taxa Simples = {taxaSimples}')
print(f'Taxa Composto = {taxaComposto}')
print(f'Taxa diferença = {taxaDiferenca}')
print(f'Capital = {capital}')
print(f'Montante = {montante}')
print(f'Juros = {juros}')
