from math import pow

pv = 4000.0

print('letra a'.upper())
i = 4 / 100.0
n = 3
potencia = pow(1+i, n)
fv = pv * potencia

print(f'pv = {pv:.2f}')
print(f'i = {i:.4f}')
print(f'n = {n}')
print(f'fv = {fv:.2f}')

print('-'*30)

print('letra b'.upper())
i = 2 / 100.0
n = 120
potencia = pow(1+i, n)
fv = pv * potencia

print(f'pv = {pv:.2f}')
print(f'i = {i:.4f}')
print(f'n = {n}')
print(f'fv = {fv:.2f}')

print('-'*30)

print('letra c'.upper())
i = 0.02 / 100.0
n = 450
potencia = pow(1+i, n)
fv = pv * potencia

print(f'pv = {pv:.2f}')
print(f'i = {i:.4f}')
print(f'n = {n}')
print(f'fv = {fv:.2f}')