from math import pow, sqrt


def gerarValores(lista):
    soma = 0.0
    quantidade = 0
    
    for i in range(0, len(lista)):
        produto = 1
        quantidade += lista[i][1]
        for j in range(0, len(lista[i])):
            produto *= lista[i][j]
        soma += produto    
    
    media = soma / quantidade
    
    print(lista)
    
    print(f'Soma dos produtos = {soma}')
    print(f'Total de elementos = {quantidade}')
    print(f'Média = {media}')
    
    produtoQuadrados = 0.0
    
    for i in range(0, len(lista)):
        prodQuadrado = lista[i][1] * pow(media - lista[i][0], 2)
        print(f'prodQuadrado_{i+1} = {prodQuadrado}')
        produtoQuadrados += prodQuadrado
        
    variancia = produtoQuadrados / quantidade
    desvioPadrao = sqrt(variancia)
    print(f'Variância = {variancia}')
    print(f'Desvio padrão = {desvioPadrao}')    


lista1 = [(3, 2), (7, 20), (8, 1)]
lista2 = [(0, 2), (1, 9), (2, 2), (3, 2), (4, 1)]
lista3 = [(10, 1), (13, 1), (14, 1), (16, 7), (17, 6)]    
gerarValores(lista3)
