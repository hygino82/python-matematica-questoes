from math import pow

fv = 8000.0
i = 2 / 100.0
n = 6
potencia = pow(1 + i, n)
pv = fv / pow(1 + i, n)

print(f'fv = {fv:.2f}')
print(f'i = {i:.4f}')
print(f'n = {n}')
print(f'potencia = {potencia}')
print(f'pv = {pv:.2f}')