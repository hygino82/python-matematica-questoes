from math import log10

pv = 40000.0
i = 16 / 100.0
n = 2
potencia = pow(1+i, n)
fv = pv*potencia
juros = fv - pv

print(f'pv = {pv:.2f}')
print(f'i = {i:.4f}')
print(f'n = {n}')
print(f'fv = {fv:.2f}')
print(f'juros = {juros:.2f}')
